using PlayFab.ClientModels;
using PlayFab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginManager : MonoBehaviour
{
    [SerializeField] private bool _login = true;
    [SerializeField] private string _titleId;

    private string _customID;
    // [SerializeField] private TitleDataManager _titleDataManager;
    
    private void Awake()
    {
        if(_login)
        {
            checkPlayfabID();
            managePlayfabLogin();
        }
    }

    private void checkPlayfabID()
    {
        if (string.IsNullOrEmpty(PlayFabSettings.staticSettings.TitleId))
        {
            PlayFabSettings.staticSettings.TitleId = _titleId;
        }
    }

    private void managePlayfabLogin()
    {
        #if UNITY_ANDROID
            loginWithAndroid();
        #elif UNITY_IOS
            loginWithIOS();
        #else
            loginAsInvited();
        #endif
    }

    private void loginWithAndroid()
    {
        var androidRequest = new LoginWithAndroidDeviceIDRequest
        {
            AndroidDeviceId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true
        };

        var request = new LoginWithCustomIDRequest { CustomId = "IdTest", CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, onLoginSuccess, onLoginFailure);
    }

    private void loginWithIOS()
    {
        var iosRequest = new LoginWithIOSDeviceIDRequest
        {
            DeviceId = SystemInfo.deviceUniqueIdentifier,
            CreateAccount = true
        };
        var request = new LoginWithCustomIDRequest { CustomId = "IdTest", CreateAccount = true };

        PlayFabClientAPI.LoginWithIOSDeviceID(iosRequest, onLoginSuccess, onLoginFailure);
    }

    private void loginAsInvited()
    {
        var request = new LoginWithCustomIDRequest { CustomId = "IdTest", CreateAccount = true };
        PlayFabClientAPI.LoginWithCustomID(request, onLoginSuccess, onLoginFailure);
    }

    private void onLoginSuccess(LoginResult result)
    {
        Debug.Log("Congratulations, you made your first successful API call!");
        // _titleDataManager.GetTitleData();
    }

    private void onLoginFailure(PlayFabError error)
    {
        Debug.LogWarning("Something went wrong with your first API call. :(");
        Debug.LogWarning("Here's some debug information:");
        Debug.LogWarning(error.GenerateErrorReport());
    }
}