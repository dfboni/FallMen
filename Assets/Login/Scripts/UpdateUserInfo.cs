using PlayFab.ClientModels;
using PlayFab;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateUserInfo : MonoBehaviour
{
    // ---- Display Name ----
    [ContextMenu("Set Name")]
    public void SetDisplayName(string displayName)
    {
        var request = new UpdateUserTitleDisplayNameRequest
        {
            DisplayName = displayName
        };

        PlayFabClientAPI.UpdateUserTitleDisplayName(request, OnDisplayNameSetSuccess, OnDisplayNameSetFailure);
    }

    private void OnDisplayNameSetSuccess(UpdateUserTitleDisplayNameResult result)
    {
        Debug.Log("Display Name actualizado correctamente: " + result.DisplayName);
    }

    private void OnDisplayNameSetFailure(PlayFabError error)
    {
        Debug.LogError("Error al establecer el Display Name: " + error.ErrorMessage);
    }

    // ---- Avatar URL ----
    [ContextMenu("Guardar Avatar")]
    public void UpdateAvatarUrl(string avatarImageUrl)
    {
        var request = new UpdateAvatarUrlRequest
        {
            ImageUrl = avatarImageUrl
        };

        PlayFabClientAPI.UpdateAvatarUrl(request, OnAvatarUrlUpdatedSuccess, OnAvatarUrlUpdatedFailure);
    }

    private void OnAvatarUrlUpdatedSuccess(EmptyResponse result)
    {
        Debug.Log("URL del avatar actualizada correctamente en PlayFab");
    }

    private void OnAvatarUrlUpdatedFailure(PlayFabError error)
    {
        Debug.LogError("Error al actualizar la URL del avatar en PlayFab: " + error.ErrorMessage);
    }

    // ---- Player Data ----
    [ContextMenu("Guardar Data")]
    public void SaveAvatarUrl(string key, string value)
    {
        var request = new UpdateUserDataRequest
        {
            Data = new Dictionary<string, string>
            {
                { key, value }
            }
        };

        PlayFabClientAPI.UpdateUserData(request, OnAvatarUrlSavedSuccess, OnAvatarUrlSavedFailure);
    }

    private void OnAvatarUrlSavedSuccess(UpdateUserDataResult result)
    {
        Debug.Log("URL del avatar guardada correctamente en PlayFab");
    }

    private void OnAvatarUrlSavedFailure(PlayFabError error)
    {
        Debug.LogError("Error al guardar la URL del avatar en PlayFab: " + error.ErrorMessage);
    }
}
