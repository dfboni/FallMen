using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestToast : MonoBehaviour
{
    [SerializeField]
    private Sprite sprite;
    [ContextMenu("test")]
    private void Test()
    {
        ToastProvider.GetOrCreate().QueueToast(new Toast("Esto es un Test del toast System"));
    } 
    [ContextMenu("test Sprite")]
    private void TestSprite()
    {
        ToastProvider.GetOrCreate().QueueToast(new Toast("Esto es un Test del toast System", sprite));
    } 
}
