using UnityEngine;

[System.Serializable]
public class Toast
{
    public string Description;
    public Sprite Icon;

    public Toast(string description, Sprite icon = null)
    {
        Description = description;
        Icon = icon;
    }
}
