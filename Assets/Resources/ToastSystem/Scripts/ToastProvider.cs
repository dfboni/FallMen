using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ToastProvider 
{
    private static IToastService _toastSystem;

    private const string DEFAULT_TOAST_SERVICE_RESOURCE_PATH = "ToastSystem";

    public static IToastService GetOrCreate()
    {
        if (_toastSystem == null)
        {
            GameObject trackingResource = Resources.Load<GameObject>(DEFAULT_TOAST_SERVICE_RESOURCE_PATH);
            if (trackingResource != null)
            {
                GameObject trackingObject = GameObject.Instantiate(trackingResource);
                trackingObject.name = trackingResource.name;

                _toastSystem = trackingObject.GetComponent(typeof(IToastService)) as IToastService;
                if (_toastSystem == null)
                {
                    Debug.LogError(nameof(_toastSystem) + " interface was not found in resource instance.");
                }
            }
            else
            {
                Debug.LogError(nameof(_toastSystem) + " can't find tracking manager resource.");
            }
        }

        return _toastSystem;
    }
}
