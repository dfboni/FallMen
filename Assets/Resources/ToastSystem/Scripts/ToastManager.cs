using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ToastManager : MonoBehaviour , IToastService
{
    [SerializeField]
    private TextMeshProUGUI _descriptionText;
    [SerializeField]
    private Image _iconImage;
    [SerializeField]
    private CanvasGroup _canvasGroup;
    [SerializeField]
    private float _timeToNextToast = 1,_timeShowingToast = 5;
    private Queue<Toast> _toasts = new Queue<Toast>();

    private bool isShowingToast = false;

    #region constantes
    private const float _POSITIVE_FACTOR= 0.01f;
    private const int _POSITIVE_GOAL= 1;
    private const float _NEGATIVE_FACTOR = -0.01f;
    private const int _NEGATIVE_GOAL = 0;
    #endregion

    public void QueueToast(object dispatcher, Toast toastInfo)
    {
        _toasts.Enqueue(toastInfo);
        ProcessToast();
    }
    public void QueueToast(Toast toastInfo)
    {
        _toasts.Enqueue(toastInfo);
        ProcessToast();
    }

    private void ProcessToast()
    {
        if (isShowingToast || _toasts.Count <= 0)
        {
            return;
        }
        isShowingToast = true;
        Toast current = _toasts.Dequeue();
        ShowToast(current);
    }

    public void ShowToast(Toast toastInfo)
    {
        _descriptionText.text = toastInfo.Description;
        if (toastInfo.Icon)
        {
            _iconImage.gameObject.SetActive(true);
            _iconImage.sprite = toastInfo.Icon;
        }
        else
        {
            _iconImage.gameObject.SetActive(false);
        }
        StartCoroutine(ProcessCanvasGroupAlpha(true));
    }

    private void HideToast()
    {
        StartCoroutine(ProcessCanvasGroupAlpha(false));
    }

    private IEnumerator ProcessCanvasGroupAlpha(bool show)
    {
        int goalAlpha = _NEGATIVE_GOAL;
        float factor = _NEGATIVE_FACTOR;
        if (show)
        {
            goalAlpha = _POSITIVE_GOAL;
            factor = _POSITIVE_FACTOR;
        }

        while (_canvasGroup.alpha != goalAlpha)
        {
            _canvasGroup.alpha += factor;
            yield return new WaitForFixedUpdate();
        }

        if (show)
        {
            yield return new WaitForSeconds(_timeShowingToast);
            HideToast();
        }
        else
        {
            yield return new WaitForSeconds(_timeToNextToast);
            isShowingToast = false;
            ProcessToast();
        }   
    }
}
