using UnityEngine;

public class PlayerPrefsLocalStorageManager : MonoBehaviour, ILocalStorageManager
{
    private const string _NO_UID_VALUE_REPLACE = "TestIDValue";
    private const string _BOOL_KEY = "BoolData";

    private const int _FALSE_VALUE = 0;
    private const int _TRUE_VALUE = 1;

    // [SerializeField] private UserLoginInfo _loginInfo;

    public void SetInt(string key, int value)
    {
        string fullKey = addUsernameToPlayerPrefsKey(key);
        PlayerPrefs.SetInt(fullKey, value);
    }

    public int GetInt(string key)
    {
        string fullKey = addUsernameToPlayerPrefsKey(key);
        return PlayerPrefs.GetInt(fullKey);
    }

    public void SetString(string key, string value)
    {
        string fullKey = addUsernameToPlayerPrefsKey(key);
        PlayerPrefs.SetString(fullKey, value);
    }

    public string GetString(string key)
    {
        string fullKey = addUsernameToPlayerPrefsKey(key);
        return PlayerPrefs.GetString(fullKey);
    }

    public void SetFloat(string key, float value)
    {
        string fullKey = addUsernameToPlayerPrefsKey(key);
        PlayerPrefs.SetFloat(fullKey, value);
    }

    public float GetFloat(string key)
    {
        string fullKey = addUsernameToPlayerPrefsKey(key);
        return PlayerPrefs.GetFloat(fullKey);
    }

    public void SetBool(string key, bool value)
    {
        string fullKey = addUsernameToPlayerPrefsKey(key) + _BOOL_KEY;
        int intValue = value == false ? _FALSE_VALUE : _TRUE_VALUE;
        PlayerPrefs.SetInt(fullKey, intValue);
    }

    public bool GetBool(string key)
    {
        string fullKey = addUsernameToPlayerPrefsKey(key) + _BOOL_KEY;
        int responseValue = PlayerPrefs.GetInt(fullKey); 
        bool responseBool = responseValue == _FALSE_VALUE ? false : true;

        return responseBool;
    }

    private string addUsernameToPlayerPrefsKey(string key)
    {
        //Build the actual key which is PlayFabID+Key so that the PlayerPrefs are per user and not per device.
        // string playfabId = _loginInfo.UID;

        // if (string.IsNullOrEmpty(playfabId))
        // {
        //     Debug.Log("PlayfabID (UID) is null or empty! Using Testing user instead");
        //     playfabId = _NO_UID_VALUE_REPLACE;
        // }

        // Debug.Log(playfabId + key);
        // return playfabId + key;

        return key;
    }
}   