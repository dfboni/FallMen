public interface ILocalStorageManager
{
    public int GetInt(string key);
    public void SetInt(string key, int value);
    
    public string GetString(string key);
    public void SetString(string key, string value);
    
    public float GetFloat(string key);
    public void SetFloat(string key, float value);

    public bool GetBool(string key);
    public void SetBool(string key, bool value);
}
