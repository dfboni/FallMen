using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleDataDeserializedStoreLinks
{
    public string AppStoreLink { get; set; }
    public string PlayStoreLink { get; set; }
    public string WebGLLink { get; set; }
}
