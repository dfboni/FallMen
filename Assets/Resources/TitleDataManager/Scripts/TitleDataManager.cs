using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using Version = TGA.Template.ForceUpdates.Core.Version;

public class TitleDataManager : MonoBehaviour
{
    [Header("Object References")]
    [SerializeField] private GameObject _loadingScreenRef = null;

    //Private Properties
    //private const string _TITLE_DATA_VERSION_JSON_NAME = "version";
    private const string _TITLE_DATA_STORE_LINKS_JSON_NAME = "StoreLinks";

    private bool _hasAlreadyFetchedTitleData = false;

    private bool _isTimedOut = false;
    private float _timeoutSeconds = 5;

    private Dictionary<string, string> _titleData = new Dictionary<string, string>();

    //Public Properties
    public event Action OnFinishGettingTitleDataSuccess;
    public event Action OnFinishGettingTitleDataFailed;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    public void GetTitleData()
    {
        if (_hasAlreadyFetchedTitleData)
        {
            OnFinishGettingTitleDataSuccess?.Invoke();
            return;
        }

        enableLoadingScreen(true);

        StartCoroutine(timeoutCorrutine());
        PlayFabClientAPI.GetTitleData(new GetTitleDataRequest(), onGetTitleDataSuccessHandler, onGetTitleDataFailedHandler);
    }

    public T DeserializeTitleData<T>(string jsonName) where T : class
    {
        T deserializedData = null;

        if (_titleData.ContainsKey(jsonName) && !string.IsNullOrEmpty(_titleData[jsonName]))
        {
            if (_titleData.ContainsKey(jsonName))
            {
                string jsonData = _titleData[jsonName];
                deserializedData = JsonConvert.DeserializeObject<T>(jsonData);

                Debug.Log($"Deserialized data : {deserializedData}");
            }
            else 
            {
                Debug.LogError($"Warning : Title Data does not contains a Json called {jsonName}");
            }
        }
        else
        {
            Debug.LogError($"Warning : Title Data does not contains a Json called {jsonName}");
        }

        return deserializedData;
    }

    //public Version GetDesiredVersion()
    //{
    //    return DeserializeTitleData<Version>(_TITLE_DATA_VERSION_JSON_NAME);
    //}

    public TitleDataDeserializedStoreLinks GetStoreLinks()
    {
        return DeserializeTitleData<TitleDataDeserializedStoreLinks>(_TITLE_DATA_STORE_LINKS_JSON_NAME);
    }

    private void onGetTitleDataSuccessHandler(GetTitleDataResult result)
    {
        _hasAlreadyFetchedTitleData = true;

        StopAllCoroutines();

        if (result.Data == null)
        {
            Debug.LogWarning("Warning : Playfab title data result is null...");
            OnFinishGettingTitleDataFailed?.Invoke();
        }

        _titleData = result.Data;

        enableLoadingScreen(false);
        OnFinishGettingTitleDataSuccess?.Invoke();
    }

    private void onGetTitleDataFailedHandler(PlayFabError error)
    {
        if (_isTimedOut) //It means that it has already timedOut 
        {
            return;
        }

        StopAllCoroutines();

        enableLoadingScreen(false);
        OnFinishGettingTitleDataFailed?.Invoke();
    }

    private void enableLoadingScreen(bool enable)
    {
        if(_loadingScreenRef != null)
        {
            _loadingScreenRef.SetActive(enable);
        }
    }

    private IEnumerator timeoutCorrutine()
    {
        yield return new WaitForSeconds(_timeoutSeconds);
        Debug.LogWarning("WARNING : Fetching playfab title data timeOut");

        _isTimedOut = true;

        enableLoadingScreen(false);
        OnFinishGettingTitleDataFailed?.Invoke();
    }
}
